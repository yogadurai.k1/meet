<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use App\Models\User;
use App\Models\Appointment;
use App\Models\Setting;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{


    public function __construct()
    {
        $this->user                  = new \App\Models\User();
    }

    // public function users(){
    //     $temp['result'] = User::select('_id', 'name', 'email', 'frg', 'isEmailCustomized','isEmailVerified', 'meetingLink', 'roles', '_class')->get();
    //     return response()->json([
    //         'error'=>false,
    //         'offers'=> $temp
    //     ]);
    // }

    public function profile(){
        $userData = JWTAuth::parseToken()->authenticate();
        if($userData){
            return response()->json([
                'status' => 1,
                'message' => 'User data successfully',
                'data' => $userData,
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Token invalid.',
            ], 400);
        }
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->messages()], 400);
        }

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'status' => 0,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => 0,
                'message' => 'Could not create token.',
            ], 500);
        }

        return response()->json([
            'status' => 1,
            'message' => 'Login sucessfully',
            'token' => $token,
        ]);

        //return $credentials;
    }

    public function googleSignin(Request $request){
        $credentials = $request->only('email');
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->messages()], 400);
        }

        try {
            $user = User::where('email', $request->email)->where('type', 'google')->first();
            if($user){
                $token = JWTAuth::fromUser($user);
                return response()->json([
                    'status' => 1,
                    'message' => 'Login sucessfully',
                    'token' => $token,
                ]); 
            } else {
                return response()->json([
                    'status' => 0,
                    'message' => 'Invalid credentials',
                ], 400);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => 0,
                'message' => 'Invalid credentials',
            ], 400);
        }

        //return $credentials;
    }

    public function get_user(Request $request){
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        $user = JWTAuth::authenticate($request->token);
 
        return response()->json(['user' => $user]);
    }

    public function register(Request $request)
    {
        $result=[];
        $requestData = [];
        
    	//Validate data
        $data = $request->only('name', 'email', 'password');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->messages()], 200);
        }
        $requestData = $request->input();
      
        $user=User::where('email',$request->email)->first();
        if(!empty($user))
        {
            $result['status']=0;
            $result['message']="Email id already exists!";
        }
        else
        {  
            //$token = JWTAuth::attempt($data);
            $requestData['isEmailVerified']=false;
            $requestData['isEmailCustomized']=false;
            $requestData['type']="default";
            $update_query = ["_id" => ""]; 
            $user = $this->user->createOrUpdate($update_query, $requestData);
            $token = JWTAuth::fromUser($user);
            $result['status']=1;
            $result['message']="New User created successfully!";
            $result['token']=$token;
        }
     

        //User created, return success response
        return response()->json([
               'data' => $result
        ], Response::HTTP_OK);
    }

    public function googleSignup(Request $request)
    {
        $result=[];
        $requestData = [];
        
    	//Validate data
        $data = $request->only('name', 'email');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->messages()], 200);
        }
        $requestData = $request->input();
      
        $user=User::where('email',$request->email)->first();
        if(!empty($user))
        {
            $result['status']=0;
            $result['message']="Email id already exists!";
        }
        else
        {  
            //$token = JWTAuth::attempt($data);
            $requestData['isEmailVerified']=false;
            $requestData['isEmailCustomized']=false;
            $requestData['type']="google";
            $update_query = ["_id" => ""]; 
            $user = $this->user->createOrUpdate($update_query, $requestData);
            $token = JWTAuth::fromUser($user);
            $result['status']=1;
            $result['message']="New User created successfully!";
            $result['token']=$token;
        }
     

        //User created, return success response
        return response()->json([
               'data' => $result
        ], Response::HTTP_OK);
    }

    public function setting(){
        $temp['result'] = Setting::select('_id', 'privacy_policy', 'terms_condition')->get();
        return response()->json([
            'status' => 1,
            'data'=> $temp
        ]);
    }

    public function resetPassword(Request $request)
    {
        $result=[];
    	//Validate data
        $data = $request->only('email', 'new_password', 'conform_password');
        $validator = Validator::make($data, [
            'email' => 'required|email|unique:users',
            'new_password' => 'required|string|min:6|max:50',
            'conform_password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->messages()], 400);
        }
        $email=$request->email;
        $new_pass=$request->new_password;
        $con_pass=$request->conform_password;

       if($new_pass==$con_pass)
        {
            $user=User::where('email',$email)->first();
            if(!empty($user))
            {
                $user=User::where('email', $email)->update(['password'=>Hash::make($new_pass)]);
                $result['status']=1;
                $result['result']="Success! Password Updated";
            }
            else
            {
                $result['status']=0;
                $result['result']="Invalid Email Credentials!";
            }
          }
        else
        {
            $result['status']=0;
            $result['result']="Password mismatched!";

        }
       
        return response()->json([
            'data' => $result
        ], Response::HTTP_OK);
    }

    


    // public function get_token(){
    //     $user = User::select('_id', 'name', 'email', 'frg', 'isEmailCustomized','isEmailVerified', 'meetingLink', 'roles', '_class')->where('email', 'nidesh.arumugam@siamcomputing.com')->first();
    //     return $user->createToken('tokens')->plainTextToken;
    // }
}
