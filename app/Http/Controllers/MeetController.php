<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JWTAuth;
use App\Models\User;
use App\Models\Appointment;
use App\Models\AvailabilityConfig;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class MeetController extends Controller
{
    public function __construct()
    {
        
    }

    public function appointments(){
     
        $temp['result'] = Appointment::select('_id')->get();
        return response()->json([
            'error'=>false,
            'offers'=> $temp
        ]);
    }

    public function branding(Request $request)
    {
    	//Validate data
        $data = $request->only('emaillogo', 'emailfooter');
        $validator = Validator::make($data, [
            'emaillogo' => 'required|string',
            'emailfooter' => 'required',
            ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }
        $email=JWTAuth::parseToken()->authenticate()->email;
        $user = User::where('email', $email)
        ->update([
            'emailLogo' => $request->emaillogo,
            'emailFooter' => $request->emailfooter
         ]);
    
        //User created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Branding updated successfully',
            'data' => $user
        ], Response::HTTP_OK);
    }

    public function meeting(Request $request)
    {
    	//Validate data
        $data = $request->only('name','description', 'duration','start_hr','end_hr','meet_color');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required',
            'duration' => 'required',
            'start_hr' => 'required',
            'end_hr' => 'required',
            'meet_color' => 'required',
          ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }
      //  return $request->meet_color;
        //Request is valid, create new user
      //  $meeting=AvailabilityConfig::select('')
        $meeting = AvailabilityConfig::create([
        	'name' => $request->name,
        	'desc' => $request->description,
        	'title' => $request->name,
            'start_hr' => $request->start_hr,
            'end_hr' => $request->end_hr,
            'meet_color' => $request->meet_color,
            'uniName' => $request->name,
        ]);

        //User created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Meeting created successfully',
            'data' => $meeting
        ], Response::HTTP_OK);
    }
 
}
